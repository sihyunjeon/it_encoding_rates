IDIR=.
CC=g++
CFLAGS=-I$(IDIR) `root-config --cflags --glibs` -g

# PYBIND=`python -m pybind11 --includes` -fPIC --shared

# PYBINDIR=nanocppfw
BINDIR=bin
ODIR=obj
SRCDIR=src
INCDIR=interface
LIBS=-lm

_OBJ = Util.o ModuleTTreeUnpacker.o PixelModule.o QCoreFactory.o QCore.o RD53StreamEncoder.o AuroraFormatter.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

# Set up output directories
$(shell   mkdir -p $(BINDIR))
$(shell   mkdir -p $(ODIR))

$(ODIR)/%.o: $(SRCDIR)/%.cc
	$(CC) -c -o $@ $< $(CFLAGS) -fPIC

.PHONY: clean default

default: link_occupancy

link_occupancy: $(OBJ)
	$(CC) src/link_occupancy.cc -o $(BINDIR)/link_occupancy $^ $(CFLAGS) $(LIBS)

clean:
	rm -f $(ODIR)/*.o *~ core $(INCDIR)/*~ $(BINDIR)/*

