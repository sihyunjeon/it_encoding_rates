#ifndef UTIL_H
#define UTIL_H

#include <vector>
#include <stdint.h>
#include <assert.h>
#include <math.h>
#include <algorithm>    // std::reverse
using namespace std;


typedef vector<vector<uint32_t>> IntMatrix;
extern IntMatrix empty_matrix(uint32_t nrows, uint32_t ncols);

extern vector<bool> adc_to_binary(uint32_t adc);

extern vector<bool> int_to_binary(int an_int, int length);

extern int binary_to_int(vector<bool> bin);

#endif // UTIL_H
