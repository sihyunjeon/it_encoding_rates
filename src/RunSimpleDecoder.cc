#include <iostream>
#include<include/cxxopts.hpp>
#include<interface/SimpleStreamDecoder.h>
#include<interface/DecoderBase.h>
#include <vector>

using namespace std;
void print_bits(vector<bool> bits){
    cout << "Printing " << bits.size() << " bits." << endl;
    for(auto bit : bits) {
        cout << (bit ? "1" : "0");
    }
    cout <<endl;
}


int main(int argc, char *argv[]){
    cxxopts::Options options("RunSimpleDecoder", "One line description of MyProgram");
    options.add_options()
    ("f,file", "Input file",cxxopts::value<string>())
    ;
    auto opts = options.parse(argc, argv);

    SimpleStreamDecoder decoder;
    decoder.load_file(opts["file"].as<string>());

    decoder.decode();
}