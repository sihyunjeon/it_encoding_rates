#include<TFile.h>

#include<include/cxxopts.hpp>

#include<interface/ModuleTTreeUnpacker.h>
#include<interface/QCoreFactory.h>
#include<interface/RD53StreamEncoder.h>
#include<interface/AuroraFormatter.h>

using namespace std;

cxxopts::ParseResult cli(int argc, char *argv[]){
    cxxopts::Options options("ITRate", "One line description of MyProgram");
    options.add_options()
    ("file_in", "File to process",cxxopts::value<string>())
    ("start", "Event index to start on",cxxopts::value<int>())
    ("stop", "Event index to stop on",cxxopts::value<int>())
    ("module", "Event index to stop on",cxxopts::value<int>())
    ("file_out", "Output file path",cxxopts::value<string>())
    ;
    auto opts = options.parse(argc, argv);
    return opts;
};

int const MAX_ENTRY=10000;
struct output_container {
    Int_t event;
    Int_t ndigis_on_det;
    Int_t nchip;

    Int_t px[MAX_ENTRY];
    Int_t py[MAX_ENTRY];
    Int_t pcharge[MAX_ENTRY];
    Int_t stream_size_raw;
    Int_t stream_size_aurora;
    Int_t stream_size_aurora_pad;
    Int_t stream_size_chip_raw;
    Int_t stream_size_chip_aurora;
    Int_t stream_size_chip_aurora_pad;

    void reset(){
        event = 0;
        ndigis_on_det = 0;
        stream_size_raw = 0;
        stream_size_aurora = 0;
        stream_size_aurora_pad = 0;
        stream_size_chip_raw = 0;
        stream_size_chip_aurora = 0;
        stream_size_chip_aurora_pad = 0;
    }
};

int main(int argc, char *argv[]){
    auto opts = cli(argc, argv);
    string treepath("BRIL_IT_Analysis/Digis");

    // Input
    ModuleTTreeUnpacker unpacker(opts["file_in"].as<string>().c_str(), treepath);

    // Output
    output_container oc;
    TFile * outfile = new TFile("formatConversion.root","RECREATE");
    TTree * tree = new TTree();
    tree->Branch("event",           &oc.event,         "event/I");
    tree->Branch("ndigis_on_det",          &oc.ndigis_on_det,          "ndigis_on_det/I");
    tree->Branch("nchip",          &oc.nchip,          "nchip/I");
    tree->Branch("stream_size_raw",        &oc.stream_size_raw,        "stream_size_raw/I");
    tree->Branch("stream_size_aurora",     &oc.stream_size_aurora,     "stream_size_aurora/I");
    tree->Branch("stream_size_aurora_pad", &oc.stream_size_aurora_pad, "stream_size_aurora_pad/I");

    tree->Branch("stream_size_chip_raw",        &oc.stream_size_chip_raw,        "stream_size_chip_raw/I");
    tree->Branch("stream_size_chip_aurora",     &oc.stream_size_chip_aurora,     "stream_size_chip_aurora/I");
    tree->Branch("stream_size_chip_aurora_pad", &oc.stream_size_chip_aurora_pad, "stream_size_chip_aurora_pad/I");

    tree->Branch("px",              oc.px,             "px[ndigis_on_det]/I");
    tree->Branch("py",              oc.py,             "py[ndigis_on_det]/I");
    tree->Branch("pcharge",         oc.pcharge,        "pcharge[ndigis_on_det]/I");
    tree->SetDirectory(outfile);
    // Processing for stream size
    QCoreFactory qfactory;
    RD53StreamEncoder encoder;
    AuroraFormatter aurora;
    int nevents = 0;
    while(unpacker.next_event()) {
        oc.reset();
        nevents++;

        if(nevents%100 == 0) {
            cout<<nevents<<endl;
        }
        int event = unpacker.get_event();

        int module_index = opts["module"].as<int>();

        auto pm = unpacker.get_module(module_index);

        if(nevents == 1){
            cout << "Module " << module_index << ", barrel " << pm.barrel << ", disk" << pm.disk << ", layer" << pm.layer << endl;
        }
        // Input reformatting + pass-throguh to output file
        for(int i=0; i<pm.adc.size(); i++) {
            oc.px[oc.ndigis_on_det] = pm.row.at(i);
            oc.py[oc.ndigis_on_det] = pm.column.at(i);
            int charge = pm.adc.at(i) * 1.5e3;
            oc.pcharge[oc.ndigis_on_det] = charge;
            oc.ndigis_on_det++;
        }

        // stream size calculation
        auto qcores = qfactory.from_pixel_module(pm);
        encoder.reset();
        encoder.serialize_event(qcores, 1);

        auto stream_raw = encoder.get_stream();
        oc.stream_size_raw = stream_raw.size();

        auto stream_aurora = aurora.apply_blocking(stream_raw, -1);
        oc.stream_size_aurora = stream_aurora.size();

        auto stream_aurora_pad = aurora.orphan_pad(stream_aurora);
        oc.stream_size_aurora_pad = stream_aurora_pad.size();


        auto qcores_per_chip = qfactory.from_pixel_module_perchip(pm);
        oc.nchip = qcores_per_chip.size();
        for(int ichip = 0; ichip<qcores_per_chip.size(); ichip++) {
            encoder.reset();
            encoder.serialize_event(qcores_per_chip.at(ichip), 1);

            auto stream_chip_raw = encoder.get_stream();
            oc.stream_size_chip_raw += stream_chip_raw.size();

            auto stream_chip_aurora = aurora.apply_blocking(stream_chip_raw, 1);
            oc.stream_size_chip_aurora += stream_chip_aurora.size();

            auto stream_chip_aurora_pad = aurora.orphan_pad(stream_chip_aurora);
            oc.stream_size_chip_aurora_pad += stream_chip_aurora_pad.size();
        }

        tree->Fill();
    }

    tree->Write("t");
    outfile->Close();
}